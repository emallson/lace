extern crate priority_queue;
extern crate noisy_float;
#[macro_use]
extern crate slog;
extern crate slog_stdlog;

pub mod greedy;
pub mod vector;
pub mod objective;
pub mod prelude;
