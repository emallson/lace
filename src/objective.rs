use noisy_float::prelude::*;
use vector::{ScaleVector, Vector};

/// Represents the optimization objective.
///
/// Typically, this is used with a constant instance of an `Objective` object that tracks important state for
/// efficient computation on a mutable instance of the associated `State` object. When an element
/// is added to the solution, `insert_mut` is used to update the state to reflect this insertion.
pub trait Objective {
    type State: Clone;

    /// Construct a new state corresponding to an empty solution.
    fn empty_state(&self) -> Self::State;

    /// The number of elements in the domain.
    fn domain_size(&self) -> usize {
        self.domain().count()
    }

    /// The integral version of the domain. Does not need to be contiguous.
    fn domain(&self) -> Box<Iterator<Item = usize>>;

    /// Compute the value of the solution vector `sol`.
    fn benefit<V: Vector + Sync>(&self, sol: &V, state: &Self::State) -> R64;

    /// Compute the marginal value of adding `el` to `sol`. `state` corresponds to `sol`, not `sol
    /// + el`.
    fn delta<V: Vector + Sync>(&self, el: usize, sol: &V, state: &Self::State) -> R64;

    /// Update `state` to reflect the insertion of `el` into `sol`. `sol` should have the element
    /// `el` already inserted.
    fn insert_mut<V: Vector + Sync>(&self, el: usize, sol: &V, state: &mut Self::State);

    /// Compute the set of elements that, if added to the solution, could cause the marginal value
    /// of `el` to change.
    fn depends(&self, el: usize, _state: &Self::State) -> Box<Iterator<Item = usize>> {
        Box::new(self.domain().filter(move |&u| u != el))
    }
}

/// Additional operations for the insertion of multiple copies of a single element in one step.
pub trait ScaledOps: Objective {
    fn scaled_delta<V: Vector + Sync>(&self, v: &ScaleVector, sol: &V, state: &Self::State) -> R64;
    fn scaled_insert_mut<V: Vector + Sync>(&self,
                                           v: &ScaleVector,
                                           sol: &V,
                                           state: &mut Self::State);
}

/// Additional operation for the insertion of an arbitrary vector in a single step.
pub trait BulkInsertion: Objective {
    fn bulk_insert_mut<U: Vector + Sync, V: Vector + Sync>(&self,
                                                           v: &U,
                                                           sol: &V,
                                                           state: &mut Self::State);
}
