#![deny(unused_extern_crates)]
extern crate bit_set;
extern crate lace;
extern crate docopt;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate capngraph;
extern crate petgraph;
extern crate rayon;
extern crate rand;
extern crate rand_mersenne_twister;
extern crate statrs;
#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate slog_async;
extern crate slog_json;
#[cfg(test)]
#[macro_use]
extern crate quickcheck;

use std::ops::Index;
use std::fs::File;
use std::iter::FromIterator;
use petgraph::prelude::*;
use petgraph::visit::*;
use rayon::prelude::*;
use lace::prelude::*;
use std::collections::{HashMap, BTreeMap};
use rand_mersenne_twister::{mersenne, MTRng64};
use std::cell::{Cell, RefCell};
use statrs::distribution::Uniform;
use rand::distributions::IndependentSample;
use bit_set::BitSet;
use slog::{Logger, Drain};
use std::sync::{Arc, RwLock};

#[cfg_attr(rustfmt, rustfmt_ignore)]
const USAGE: &str = "
Run the pseudo-IM simulation. We use the IC model.

The gamma-brute and gamma-s-brute commands are used to find values of γ_d
and γ_s, respectively. They are naïve brute-force methods and will not complete
quickly.

Usage:
    inf <graph> <k> [options]
    inf gamma-brute <graph> <k> [options]
    inf gamma-s-brute <graph> <k> [options]
    inf (-h | --help)

Options:
    -h --help               Show this screen.
    --samples <sam>         The number of samples used for Monte Carlo. [default: 10000]
    --threads <t>           The number of threads to use. [default: 1]
    --log <file>            Log output to <file> in JSON lines format.
    --box <constraint>      Bound on the number of times a single element can be chosen. 
                            If not set, an element may be selected an arbitrary number of times.
    --greedy <method>       Greedy method to use. [default: standard]
    --delta <delta>         Parameter for Fast Greedy (description pending).
    --epsilon <eps>         Error margin for the threshold greedy method.
    --det-steps <bmax>      Require <bmax> steps to deterministically seed an element. [default: 10]
    --kappa <kappa>         Step size between thresholds.
";

#[derive(Serialize, Deserialize, Debug)]
struct Args {
    cmd_gamma_brute: bool,
    cmd_gamma_s_brute: bool,
    arg_graph: String,
    arg_k: usize,
    flag_box: Option<usize>,
    flag_samples: usize,
    flag_threads: usize,
    flag_log: Option<String>,
    flag_greedy: GreedyMethod,
    flag_kappa: Option<f64>,
    flag_delta: Option<f64>,
    flag_epsilon: Option<f64>,
    flag_det_steps: usize,
}

#[derive(Serialize, Deserialize, Debug)]
enum GreedyMethod {
    Standard,
    Threshold,
    Simple,
    Fast,
}

thread_local! {
    static RNG: RefCell<MTRng64> = RefCell::new(mersenne());
}

type InfGraph = Graph<f32, f32>;

#[derive(Debug, Clone)]
pub struct SampleGraph {
    node_weights: Vec<f32>,
    edge_weights: Vec<f32>,
}

impl Index<NodeIndex> for SampleGraph {
    type Output = f32;

    fn index(&self, idx: NodeIndex) -> &f32 {
        &self.node_weights[idx.index()]
    }
}

impl Index<EdgeIndex> for SampleGraph {
    type Output = f32;

    fn index(&self, idx: EdgeIndex) -> &f32 {
        &self.edge_weights[idx.index()]
    }
}

fn flat_node_progression(base: f32, count: usize, prog: f32) -> f32 {
    base + count as f32 * prog
}

fn flat_edge_progression(base: f32, count: usize, prog: f32) -> f32 {
    base + count as f32 * prog 
}

#[allow(dead_code)]
fn exp_node_progression(base: f32, count: usize, prog: f32) -> f32 {
    base * prog.powi(count as i32)
}

#[allow(dead_code)]
fn exp_edge_progression(base: f32, count: usize, prog: f32) -> f32 {
    base * prog.powi(count as i32)
}

/// Returns `true` if with the given `weight` and `rand`om number, an item should become active
fn active(weight: f32, rand: f32) -> bool {
    rand <= weight
}

#[derive(Clone, Default)]
struct State {
    samples: Vec<SampleGraph>,
    active: Vec<BitSet>,
    prog: f32,
    benefit_calls: Cell<usize>,
    delta_calls: Cell<usize>,
    updates: usize,
}

impl State {
    fn new(basis: &InfGraph, samples: usize, prog: f32) -> Self {
        let mut s = State { prog, ..Default::default() };
        s.resample(basis, samples, &NullVector);
        s
    }

    fn resample<V: Vector + Sync>(&mut self, basis: &InfGraph, samples: usize, sol: &V) {
        self.samples.clear();
        self.samples.reserve_exact(samples);
        self.active.clear();
        self.active.reserve_exact(samples);

        let dist = Uniform::new(0.0, 1.0).unwrap();

        (0..samples)
            .into_par_iter()
            .map(|_| 
                 SampleGraph {
                     node_weights: basis.node_indices().map(|_| RNG.with(|rng| dist.ind_sample(&mut *rng.borrow_mut())) as f32).collect(),
                     edge_weights: basis.edge_indices().map(|_|RNG.with(|rng| dist.ind_sample(&mut *rng.borrow_mut())) as f32).collect()
                 })
            .collect_into(&mut self.samples);

        self.reactivate(basis, sol)
    }

    fn reactivate<V: Vector + Sync>(&mut self, basis: &InfGraph, sol: &V) {
        let prog = self.prog;
        self.samples.par_iter()
            .map(|sample| {
                active_nodes(basis, sample, sol, prog)
            }).collect_into(&mut self.active);
    }
}

/// Calculates the set of active nodes given a solution `sol` on a sample graph `sample`.
///
/// This is done by first identifying which nodes are seeds on this sample, then running a BFS
/// across the sub-graph induced by the live edge set.
fn active_nodes<V: Vector>(basis: &InfGraph, sample: &SampleGraph, sol: &V, prog: f32) -> BitSet {
    let mut set = BitSet::new();
    for (idx, &basis_weight) in basis.node_references() {
        // a node is active iff the random weight assigned to it in the same is <= the value given
        // by the progression function. Thus, a node may not be activated even if it has a positive
        // associated count.
        let weight = flat_node_progression(basis_weight, sol.get(idx.index()), prog);
        let rand = sample[idx];
        if active(weight, rand) {
            set.insert(idx.index());
        }
    }
    if set.is_empty() {
        // println!("no active nodes in sample");
        return set;
    } 

    let filtered = EdgeFiltered::from_fn(basis, |er| {
        let weight = flat_edge_progression(*er.weight(), sol.get(er.target().index()), prog);
        let rand = sample[er.id()];
        active(weight, rand)
    });
    let bfs = multi_bfs(&filtered, set.iter().map(|i| NodeIndex::new(i)));
    set.extend(bfs.iter(&filtered).map(|n| n.index()));
    set
}

fn multi_bfs<G, I, N, VM>(g: G, starts: I) -> Bfs<N, VM>
    where N: Copy + PartialEq,
          VM: VisitMap<N>,
          G: GraphRef + Visitable<NodeId=N, Map=VM>,
          I: IntoIterator<Item=N>,
{
    let stack = std::collections::VecDeque::from_iter(starts);
    let mut discovered = g.visit_map();
    for &el in &stack {
        discovered.visit(el);
    }

    Bfs {
        stack,
        discovered
    }
}

struct InfObjective<'a> {
    g: &'a InfGraph,
    samples: usize,
    resampling: bool,
    prog: f32,
}

impl<'a> InfObjective<'a> {
    fn new(g: &'a InfGraph, samples: usize, steps: usize, no_resampling: bool) -> Self {
        Self {
            g, samples, resampling: !no_resampling, prog: 1.0 / steps as f32
        }
    }
}

impl<'a> Objective for InfObjective<'a> {
    type State = State;

    fn domain(&self) -> Box<Iterator<Item = usize>> {
        Box::new(self.g.node_indices().map(|u| u.index()))
    }

    fn empty_state(&self) -> Self::State {
        Self::State::new(self.g, self.samples, self.prog)
    }

    fn benefit<V: Vector + Sync>(&self, _sol: &V, state: &Self::State) -> R64 {
        let count = state.benefit_calls.get();
        state.benefit_calls.set(count + 1);
        r64(state.active.par_iter().map(|set| set.len() as f64).sum::<f64>() / state.active.len() as f64)
    }

    /// Compute what *would be* activated were we to increment `el`'s value in the solution. If
    /// incrementing `el`'s value would not cause it to be newly-activated on a sample, then the
    /// marginal gain is 0 for that sample.
    fn delta<V: Vector + Sync>(&self, el: usize, sol: &V, state: &Self::State) -> R64 {
        let count = state.delta_calls.get();
        state.delta_calls.set(count + 1);
        let basis = self.g;
        let el = NodeIndex::new(el);
        r64(state.samples.par_iter().zip(&state.active)
            .map(|(sample, active_set)| {
                // true if the element is already active in this sample
                let already_active = active_set.contains(el.index());
                // true if the element would be activated by external forces if we increment it
                let would_externally_activate = active(flat_node_progression(basis[el], sol.get(el.index()) + 1, self.prog), sample[el]);
                // true if the element would be activated by a neighboring node if we increment it
                let would_edge_activate = basis.edges_directed(el, Incoming).any(|er| active_set.contains(er.source().index()) && {
                    let weight = flat_edge_progression(*er.weight(), sol.get(el.index()) + 1, self.prog);
                    active(weight, sample[er.id()])
                });
                if already_active || (!would_externally_activate && !would_edge_activate) {
                    0.0
                } else {
                    let filtered = EdgeFiltered::from_fn(basis, |er| !active_set.contains(er.target().index()) && {
                        let weight = flat_edge_progression(*er.weight(), sol.get(er.target().index()), self.prog);
                        let rand = sample[er.id()];
                        active(weight, rand)
                    });
                    Bfs::new(&filtered, el).iter(&filtered).count() as f64
                }
            }).sum::<f64>() / state.samples.len() as f64)
    }

    fn insert_mut<V: Vector + Sync>(&self, _el: usize, sol: &V, state: &mut Self::State) {
        if self.resampling {
            state.resample(self.g, self.samples, sol);
        } else {
            unimplemented!()
        }
    }
}

impl<'a> ScaledOps for InfObjective<'a> {
    /// Compute what *would be* activated were we to increment `el`'s value in the solution. If
    /// incrementing `el`'s value would not cause it to be newly-activated on a sample, then the
    /// marginal gain is 0 for that sample.
    fn scaled_delta<V: Vector + Sync>(&self, v: &ScaleVector, sol: &V, state: &Self::State) -> R64 {
        let basis = self.g;
        let el = NodeIndex::new(v.el());
        let scale = v.get(v.el());
        r64(state.samples.par_iter().zip(&state.active)
            .map(|(sample, active_set)| {
                // true if the element is already active in this sample
                let already_active = active_set.contains(el.index());
                // true if the element would be activated by external forces if we increment it
                let would_externally_activate = active(flat_node_progression(basis[el], sol.get(el.index()) + scale, self.prog), sample[el]);
                // true if the element would be activated by a neighboring node if we increment it
                let would_edge_activate = basis.edges_directed(el, Incoming).any(|er| active_set.contains(er.source().index()) && {
                    let weight = flat_edge_progression(*er.weight(), sol.get(el.index()) + scale, self.prog);
                    active(weight, sample[er.id()])
                });
                if already_active || (!would_externally_activate && !would_edge_activate) {
                    0.0
                } else {
                    let filtered = EdgeFiltered::from_fn(basis, |er| !active_set.contains(er.target().index()) && {
                        let weight = flat_edge_progression(*er.weight(), sol.get(er.target().index()), self.prog);
                        let rand = sample[er.id()];
                        active(weight, rand)
                    });
                    Bfs::new(&filtered, el).iter(&filtered).count() as f64
                }
            }).sum::<f64>() / state.samples.len() as f64)
    }

    fn scaled_insert_mut<V: Vector + Sync>(&self, _v: &ScaleVector, sol: &V, state: &mut Self::State) {
        if self.resampling {
            state.resample(self.g, self.samples, sol);
        } else {
            unimplemented!()
        }
    }
}

impl<'a> BulkInsertion for InfObjective<'a> {
    fn bulk_insert_mut<U: Vector + Sync, V: Vector + Sync>(&self, _v: &U, sol: &V, state: &mut Self::State) {
        state.resample(self.g, self.samples, sol)
    }
}

fn logger(path: &Option<String>) -> Logger {
    let term = slog_term::term_compact();
    if let &Some(ref path) = path {
        let json = slog_json::Json::default(File::create(path).unwrap());
        let drain = slog_async::Async::default(slog::Duplicate(term, json).fuse()).fuse();
        Logger::root(drain, o!())
    } else {
        let drain = slog_async::Async::default(term.fuse()).fuse();
        Logger::root(drain, o!())
    }
}

const BASE: f32 = 0.01;
fn load_graph(path: &str) -> InfGraph {
    let g = capngraph::load_graph(path).expect("Unable to read graph.");
    
    // setting all nodes to be equiprobable to be externally activated
    g.map(|_, _| BASE, |_, &w| w)
}

fn brute_force_gamma_s_greedy(g: &InfGraph, samples: usize, steps: usize, k: usize, vs: Vec<DenseVector>) -> f64 {
    let obj = InfObjective::new(g, samples, steps, false);

    let values = Arc::new(RwLock::new(HashMap::default()));
    #[derive(Clone)]
    struct Value {
        f_w: R64,
        marginals: Vec<R64>,
    }

    impl Value {
        fn compute(w: &DenseVector, obj: &InfObjective, state: &mut State) -> Self {
            state.reactivate(&obj.g, w);
            Value {
                f_w: obj.benefit(w, &state),
                marginals: obj.domain().map(|i| obj.delta(i, w, &state)).collect(),
            }
        }
    }

    fn gamma_s(f_v: R64, f_w: R64, marginals: &Vec<R64>, v: &DenseVector, w: &DenseVector, obj: &InfObjective) -> R64 {
        obj.domain().map(|i| marginals[i] * (w[i] - v[i]) as f64).sum::<R64>() / (f_w - f_v)
    }

    fn dfs(g: &InfGraph, f_v: R64, marginals_v: &Vec<R64>, v: &DenseVector, cur: &mut DenseVector, state: &mut State, obj: &InfObjective, count: usize, k: usize, steps: usize, visited: Arc<RwLock<HashMap<DenseVector, Value>>>) -> R64 {
        let val = if visited.read().unwrap().contains_key(cur) {
            visited.read().unwrap()[cur].clone()
        } else {
            let val = Value::compute(cur, obj, state);
            visited.write().unwrap().insert(cur.clone(), val);
            visited.read().unwrap()[cur].clone()
        };

        let mut gs = gamma_s(f_v, val.f_w, marginals_v, v, cur, obj);
        if count < k {
            for i in obj.domain() {
                if cur[i] == steps {
                    continue;
                }
                *cur += unit(i);
                let gsp = dfs(g, f_v, marginals_v, v, cur, state, obj, count + 1, k, steps, visited.clone());
                *cur.get_mut(i).unwrap() -= 1;
                // multiple applicable types in scope for .min >.>
                if gsp < gs {
                    gs = gsp;
                }
            }
        }
        gs
    }

    let mut min_gs = 1.0;
    let domain = obj.domain().collect::<Vec<_>>();
    for (i, v) in vs.iter().enumerate() {
        let mut state = obj.empty_state();
        state.reactivate(&g, v);
        let f_v = obj.benefit(v, &state);
        let marginals = obj.domain().map(|i| obj.delta(i, v, &state)).collect();
        let gs = domain.par_iter().filter(|&&i| v[i] < steps).map(|&i| {
            let mut state = obj.empty_state();
            state.reactivate(&g, v);
            let mut cur = v.clone();
            cur += unit(i);
            dfs(&g, f_v, &marginals, v, &mut cur, &mut state, &obj, 1, k, steps, values.clone())
        }).min().unwrap().raw();

        println!("{} ({}): {}", i, v.cardinality(), gs);
        if gs < min_gs {
            min_gs = gs;
        }
    }

    return min_gs;
}

fn threshold_greedy_gamma_d(g: &InfGraph, samples: usize, steps: usize, vs: Vec<DenseVector>) -> f64 {
    let obj = InfObjective::new(g, samples, steps, false);
    let mut w_state = obj.empty_state();
    let mut v_state = obj.empty_state();

    fn gamma_d(v: &DenseVector, w: &DenseVector, obj: &InfObjective, v_state: &State, w_state: &State, steps: usize) -> R64 {
        obj.domain().filter(|&i| w[i] < steps).map(|i| obj.delta(i, w, w_state) / obj.delta(i, v, v_state)).min().unwrap()
    }

    let mut worst = r64(1.0);
    for (i, w) in vs.iter().enumerate().skip(1) {
        w_state.reactivate(g, w);
        for v in vs.iter().take(i) {
            v_state.reactivate(g, v);
            let g = gamma_d(v, w, &obj, &v_state, &w_state, steps);
            worst = Ord::min(worst, g);
        }
    }
    worst.raw()
}

fn main() {
    let args: Args = docopt::Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    rayon::initialize(rayon::Configuration::new().num_threads(args.flag_threads))
        .expect("Configuring number of rayon threads failed.");

    let log = logger(&args.flag_log);
    info!(log, "args"; "args" => serde_json::to_string(&args).unwrap());
    let g = load_graph(&args.arg_graph);
    info!(log, "loaded graph"; "nodes" => g.node_count(), "edges" => g.edge_count());

    let obj = InfObjective::new(&g, args.flag_samples, args.flag_det_steps, false);
    info!(log, "built objective");

    if args.cmd_gamma_brute {
        let (_, _, vs) = threshold_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k),
        r64(args.flag_epsilon.expect("--epsilon not set, but required for threshold greedy.")),
        r64(args.flag_kappa.expect("--kappa not set, but required for threshold greedy.")),
        true,
        Some(log.new(o!("section" => "greedy"))));

        let vlen = vs.as_ref().map(|vs| vs.len()).unwrap_or(0);
        info!(log, "solved greedily"; "vs.len()" => vlen);
        info!(log, "brute forced greedy γ_d"; "γ_d" => threshold_greedy_gamma_d(&g, args.flag_samples, args.flag_det_steps, vs.unwrap()), "vs.len()" => vlen);
        return;
    }

    if args.cmd_gamma_s_brute {
        let (_, _, vs) = match args.flag_greedy {
            GreedyMethod::Fast => fast_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), 
                                              r64(args.flag_epsilon.expect("--epsilon not set, but required for fast greedy.")),
                                              r64(args.flag_delta.expect("--delta not set, but required for fast greedy.")),
                                              r64(args.flag_kappa.expect("--kappa not set, but required for threshold greedy.")),
                                              true,
                                              Some(log.new(o!("section" => "greedy")))),
            GreedyMethod::Threshold => threshold_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), 
                                                        r64(args.flag_epsilon.expect("--epsilon not set, but required for threshold greedy.")),
                                                        r64(args.flag_kappa.expect("--kappa not set, but required for threshold greedy.")),
                                                        true,
                                                        Some(log.new(o!("section" => "greedy")))),
            GreedyMethod::Standard => std_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), true, Some(log.new(o!("section" => "greedy")))),
            _ => unimplemented!("Cannot use simple greedy with γ_s brute force"),
        };
        let vlen = vs.as_ref().map(|vs| vs.len()).unwrap_or(0);
        info!(log, "solved greedily"; "vs.len()" => vlen);
        info!(log, "brute forced greedy γ_s"; "γ_s" => brute_force_gamma_s_greedy(&g, args.flag_samples, args.flag_det_steps, args.arg_k, vs.unwrap()), "vs.len()" => vlen);
        return;
    }

    info!(log, "running greedy");
    let (sol, state, _) = match args.flag_greedy {
        GreedyMethod::Standard => std_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), false, Some(log.new(o!("section" => "greedy")))),
        GreedyMethod::Threshold => threshold_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), 
                                                    r64(args.flag_epsilon.expect("--epsilon not set, but required for threshold greedy.")),
                                                    r64(args.flag_kappa.expect("--kappa not set, but required for threshold greedy.")),
                                                    false,
                                                    Some(log.new(o!("section" => "greedy")))),
        GreedyMethod::Fast => fast_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), 
                                          r64(args.flag_epsilon.expect("--epsilon not set, but required for fast greedy.")),
                                          r64(args.flag_delta.expect("--delta not set, but required for fast greedy.")),
                                          r64(args.flag_kappa.expect("--kappa not set, but required for threshold greedy.")),
                                          false,
                                          Some(log.new(o!("section" => "greedy")))),
        GreedyMethod::Simple => {
            let (sol, state) = simple_greedy(&obj, args.arg_k, args.flag_box.unwrap_or(args.arg_k), Some(log.new(o!("section" => "greedy"))));
            (sol, state, None)
        },
    };
    info!(log, "function evaluations"; "benefit" => state.benefit_calls.get(), "delta" => state.delta_calls.get());
    let baseline = obj.benefit(&NullVector, &obj.empty_state());
    let ben = obj.benefit(&sol, &state);
    let solset = sol.iter().cloned().enumerate().filter(|&(_, c)| c > 0).collect::<BTreeMap<_, _>>();
    let solprobs = solset.iter().map(|(&node, &count)| (node, flat_node_progression(g[NodeIndex::new(node)], count, 1.0 / args.flag_det_steps as f32))).collect::<BTreeMap<_, _>>();
    info!(log, "found solution"; "value" => ben.raw() - baseline.raw(), "solution" => serde_json::to_string(&solset).unwrap(), "activation probabilities" => serde_json::to_string(&solprobs).unwrap());
}

#[cfg(test)]
mod test {
    use super::*;
    use quickcheck::TestResult;

    quickcheck! {
        fn cannot_activate_0(rand: f32) -> TestResult {
            if rand > 1.0 || rand < 0.0 {
                TestResult::discard()
            } else {
                TestResult::from_bool(rand == 0.0 || !active(0.0, rand))
            }
        }
    }

    /// Test that at least 75% of nodes have a positive marginal gain.
    /// The probability of this *not* occurring on the network used with >1k samples is vanishingly
    /// small, so this test should almost always pass.
    #[test]
    fn positive_marginal_gains() {
        let g = load_graph("ca-GrQc_uniform.bin");
        let obj = InfObjective::new(&g, 1_000, 10, false);
        let state = obj.empty_state();

        let threshold = 0.75 * g.node_count() as f64;

        assert!(obj.domain().filter(|&i| obj.delta(i, &NullVector, &state) > 0.0).count() > threshold.ceil() as usize);
    }

    /// Tests that a constructed sample has at least one active node. With the default graph
    /// settings, one in every hundred nodes (on average) is active. Thus, the probability of this
    /// test failing one or more times while it actually is working is vanishingly small.
    #[test]
    fn sample_not_empty() {
        let g = load_graph("ca-GrQc_uniform.bin");
        let sample = State::new(&g, 1, 0.1);
        assert!(sample.active[0].len() >= 1);
    }

    /// Verifies that if `active(weight, rand)` is true then `u` is in the active set.
    #[test]
    fn node_active_matches_weight() {
        let g = load_graph("ca-GrQc_uniform.bin");
        let sample = State::new(&g, 1, 0.1);

        let ref actset = sample.active[0];
        let ref sample = sample.samples[0];
        for nr in g.node_references() {
            if active(*nr.weight(), sample[nr.id()]) {
                assert!(actset.contains(nr.id().index()));
            }
        }
    }
}
