use std::iter::FromIterator;
use priority_queue::PriorityQueue;
use vector::*;
use objective::*;
use slog::{Drain, Logger};
use slog_stdlog::StdLog;
use noisy_float::prelude::*;

/// The standard greedy algorithm on the integer lattice. Locates a vector of magnitude `k` with no
/// component larger than `b`.
pub fn standard<O: Objective>(obj: &O,
                              k: usize,
                              b: usize,
                              record_intermediate_sols: bool,
                              log: Option<Logger>)
                              -> (DenseVector, O::State, Option<Vec<DenseVector>>) {
    let log = log.unwrap_or_else(|| Logger::root(StdLog.fuse(), o!()));
    let mut sol = DenseVector::new(obj.domain_size());
    let mut intermediate = if record_intermediate_sols {
        Some(vec![sol.clone()])
    } else {
        None
    };
    let mut state = obj.empty_state();
    let mut queue = PriorityQueue::from_iter(obj.domain()
        .map(|el| (el, obj.delta(el, &sol, &state))));
    debug!(log, "built initial queue, beginning loop");
    for _ in 0..k {
        if let Some((el, val)) = queue.pop() {
            debug!(log, "selected {} from top of queue", el; "value" => val.raw());
            sol += unit(el);
            if let Some(vs) = intermediate.as_mut() {
                vs.push(sol.clone());
            }
            obj.insert_mut(el, &sol, &mut state);
            for dep in obj.depends(el, &state) {
                queue.change_priority(&dep, obj.delta(dep, &sol, &state));
            }
            if sol.get(el) < b {
                queue.push(el, obj.delta(el, &sol, &state));
                debug!(log, "updated marginal gains of {} and dependents", el);
            } else {
                debug!(log, "updated marginal gains of {}'s dependents", el);
            }
        } else {
            crit!(log,
                  "reached end of queue without reaching cardinality constraint");
            break;
        }
    }
    assert!(constraints_satisfied(&sol, k, b));
    (sol, state, intermediate)
}

/// Locate the pivot for a given element `i`.
fn find_pivot<O: Objective + ScaledOps>(obj: &O,
                                        s: &DenseVector,
                                        state: &O::State,
                                        i: usize,
                                        k: usize,
                                        tau: R64)
                                        -> usize {
    let mut bs = 1;
    // let mut bt = k - s[i];
    let mut bt = k;
    while bt != bs + 1 {
        let m = (bt + bs) / 2;
        let mei = m * unit(i);
        if obj.scaled_delta(&mei, s, state) >= tau * m as f64 {
            bs = m;
        } else {
            bt = m;
        }
    }
    bs
}

/// The threshold-greedy algorithm. Originally given for lattice functions by Soma & Yoshida,
/// modified for use with non-submodular lattice functions.
pub fn threshold<O: Objective + ScaledOps>(obj: &O,
                                           k: usize,
                                           b: usize,
                                           epsilon: R64,
                                           kappa: R64,
                                           record_intermediate_sols: bool,
                                           log: Option<Logger>)
                                           -> (DenseVector, O::State, Option<Vec<DenseVector>>) {

    let log = log.unwrap_or_else(|| Logger::root(StdLog.fuse(), o!()));
    let mut sol = DenseVector::new(obj.domain_size());
    let mut intermediate = if record_intermediate_sols {
        Some(vec![sol.clone()])
    } else {
        None
    };
    let mut state = obj.empty_state();
    #[allow(non_snake_case)]
    let M = obj.domain().map(|i| obj.delta(i, &sol, &state)).max().unwrap();
    let mut tau = M;
    let mut card = 0;
    let bounds = b * obj.domain_size();

    info!(log, "beginning threshold greedy"; "initial τ" => tau.raw(), "stopping τ" => (epsilon.powi(2) * M / k as f64).raw());
    'outer: while tau >= kappa * epsilon.powi(2) * M / k as f64 {
        let card_start = card;
        for i in obj.domain() {
            let mut scale = b - sol[i]; //.min(k - card);
            if 0 == scale {
                continue; // cannot gain by considering this element
            }
            if (k - card) < scale {
                scale = k - card;
            }
            let v = unit(i) * (scale);
            let l = if obj.scaled_delta(&v, &sol, &state) >= tau * (scale) as f64 {
                scale
            } else if obj.delta(i, &sol, &state) < tau {
                0
            } else {
                find_pivot(obj, &sol, &state, i, scale, tau)
            };

            if l > 0 {
                debug!(log, "found element {} to add to solution {} times", i, l);
                card += l;
                let x = l * unit(i);
                sol += x;
                debug!(log, "performing insertion");
                obj.scaled_insert_mut(&x, &sol, &mut state);
                if let Some(vs) = intermediate.as_mut() {
                    vs.push(sol.clone());
                }

                assert!(card <= k);
                if card == k {
                    break 'outer;
                }

                if card == bounds {
                    break 'outer;
                }
            }
        }

        info!(log, "iteration complete"; "τ" => tau.raw(), "items added" => card - card_start);
        tau *= kappa.raw(); //1.0 - epsilon.raw();
    }
    // if card < k && card < bounds {
    // warn!(log, "threshold greedy did not produce solution with tight constraints; this either doesn't matter (because γ > 0 and >> machine-epsilon) or does matter but γ is so small that we won't bother since we realistically have no guarantees."; "γ" => gamma.raw());
    // }
    assert!(constraints_satisfied(&sol, k, b));
    (sol, state, intermediate)
}

pub fn simple<O: Objective + BulkInsertion>(obj: &O,
                                            k: usize,
                                            b: usize,
                                            log: Option<Logger>)
                                            -> (DenseVector, O::State) {
    let log = log.unwrap_or_else(|| Logger::root(StdLog.fuse(), o!()));
    let mut sol = DenseVector::new(obj.domain_size());
    let mut state = obj.empty_state();
    let mut queue = PriorityQueue::from_iter(obj.domain()
        .map(|el| (el, obj.delta(el, &sol, &state))));
    debug!(log, "built initial queue, beginning loop");
    let mut card = 0;
    let mut els = vec![];
    while card < k {
        if let Some((el, val)) = queue.pop() {
            debug!(log, "selected {} from top of queue", el; "value" => val.raw());
            let count = b.min(k - card);
            sol += count * unit(el);
            card += count;
            els.push(el);
        } else {
            crit!(log,
                  "reached end of queue without reaching cardinality constraint");
            break;
        }
    }
    assert!(constraints_satisfied(&sol, k, b));
    obj.bulk_insert_mut(&sol, &sol, &mut state);
    (sol, state)
}

pub fn fast<O: Objective + ScaledOps>(obj: &O,
                                      k: usize,
                                      b: usize,
                                      eps: R64,
                                      delta: R64,
                                      kappa: R64,
                                      record_intermediate_sols: bool,
                                      log: Option<Logger>)
                                      -> (DenseVector, O::State, Option<Vec<DenseVector>>) {
    assert!(delta > 0.0 && delta < 1.0);
    let log = log.unwrap_or_else(|| Logger::root(StdLog.fuse(), o!()));
    let mut sol = DenseVector::new(obj.domain_size());
    let mut intermediate = if record_intermediate_sols {
        Some(vec![sol.clone()])
    } else {
        None
    };
    let mut state = obj.empty_state();
    // let default_beta = r64(1.0);
    // let mut beta = Splittable::Split(vec![default_beta; obj.domain_size()]);
    #[allow(non_snake_case)]
    let mut M = r64(0.0);
    let mut m;
    for el in obj.domain() {
        m = obj.delta(el, &sol, &state) / kappa;
        if m > M {
            M = m;
        }
    }

    debug!(log, "built initial queue, beginning loop");
    let mut card = 0;
    let mut beta = r64(1.0);
    let mut mp = M / kappa;
    while card < k {
        let (_, m) = obj.domain()
            .map(|j| (j, obj.delta(j, &sol, &state)))
            .max_by_key(|&(_, delta_j)| delta_j)
            .expect("domain does not have a maximum -- likely due to empty domain");
        if m < M * eps * eps / k as f64 {
            break;
        }

        if m > mp * kappa {
            beta *= delta;
        }

        mp = m;
        let card_start = card;
        let tau = m * beta * kappa;
        for i in obj.domain() {
            let mut scale = b - sol[i]; //.min(k - card);
            if 0 == scale {
                continue; // cannot gain by considering this element
            }
            if (k - card) < scale {
                scale = k - card;
            }
            let v = unit(i) * (scale);
            let l = if obj.scaled_delta(&v, &sol, &state) >= tau * (scale) as f64 {
                scale
            } else if obj.delta(i, &sol, &state) < tau {
                0
            } else {
                find_pivot(obj, &sol, &state, i, scale, tau)
            };

            if l > 0 {
                debug!(log, "found element {} to add to solution {} times", i, l);
                card += l;
                let x = l * unit(i);
                sol += x;
                debug!(log, "performing insertion");
                obj.scaled_insert_mut(&x, &sol, &mut state);

                assert!(card <= k);
                if card == k {
                    break;
                }
            }
        }

        info!(log, "iteration complete"; "τ" => tau.raw(), "items added" => card - card_start, "β" => beta.raw());
        if let Some(vs) = intermediate.as_mut() {
            vs.push(sol.clone());
        }
    }
    info!(log, "fast greedy complete"; "β" => beta.raw());
    (sol, state, intermediate)
}

fn constraints_satisfied(sol: &DenseVector, k: usize, b: usize) -> bool {
    let mut sum = 0;
    for &el in sol.iter() {
        if el > b {
            return false;
        }
        sum += el;
    }

    sum <= k
}

#[cfg(test)]
mod test {
    use super::*;

    /// An objective where marginal gains don't change. This allows us to very easily perform basic
    /// sanity checks on the output of our methods.
    struct IndependentObjective {
        low: usize,
        high: usize,
    }

    impl Objective for IndependentObjective {
        type State = ();
        fn domain(&self) -> Box<Iterator<Item = usize>> {
            Box::new(0..self.high + 1 - self.low)
        }

        fn domain_size(&self) -> usize {
            self.high + 1 - self.low
        }

        fn benefit<V: Vector + Sync>(&self, sol: &V, _state: &()) -> R64 {
            r64(self.domain()
                .map(|el| (el + self.low) * sol[el])
                .sum::<usize>() as f64)
        }

        fn empty_state(&self) -> () {
            ()
        }

        fn delta<V: Vector + Sync>(&self, el: usize, _sol: &V, _state: &()) -> R64 {
            r64((el + self.low) as f64)
        }

        fn insert_mut<V: Vector + Sync>(&self, _el: usize, _sol: &V, _state: &mut ()) {}
    }

    impl ScaledOps for IndependentObjective {
        fn scaled_delta<V: Vector + Sync>(&self, v: &ScaleVector, _sol: &V, _state: &()) -> R64 {
            self.benefit(v, &())
        }

        fn scaled_insert_mut<V: Vector + Sync>(&self,
                                               _v: &ScaleVector,
                                               _sol: &V,
                                               _state: &mut ()) {
        }
    }

    #[test]
    fn std_greedy_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = standard(&obj, 10, 10, false, None);
        let expected = DenseVector::new(11) + 10 * unit(10);
        assert_eq!(sol, expected);
    }

    #[test]
    fn std_greedy_bounded_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = standard(&obj, 10, 3, false, None);
        let expected = DenseVector::new(11) + 3 * unit(10) + 3 * unit(9) + 3 * unit(8) + unit(7);
        assert_eq!(sol, expected);
    }

    #[test]
    fn std_greedy_absurd_cardinality_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = standard(&obj, 20, 1, false, None);
        let expected = vec![1; 11].into();
        assert_eq!(sol, expected);
    }

    #[test]
    fn threshold_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = threshold(&obj, 10, 10, r64(0.01), r64(0.9), false, None);
        let expected = DenseVector::new(11) + 10 * unit(10);
        assert_eq!(sol, expected);
    }

    #[test]
    fn threshold_bounded_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = threshold(&obj, 10, 3, r64(0.01), r64(0.9), false, None);
        let expected = DenseVector::new(11) + 3 * unit(10) + 3 * unit(9) + 3 * unit(8) + unit(7);
        assert_eq!(sol, expected);
    }

    #[test]
    fn threshold_absurd_cardinality_sanity() {
        let obj = IndependentObjective { low: 0, high: 10 };
        let (sol, _, _) = threshold(&obj, 20, 1, r64(0.01), r64(0.9), false, None);
        let mut expected: DenseVector = vec![1; obj.domain_size()].into();
        expected.set(0, 0);
        assert_eq!(sol, expected);
    }
}
