pub use greedy::standard as std_greedy;
pub use greedy::threshold as threshold_greedy;
pub use greedy::simple as simple_greedy;
pub use greedy::fast as fast_greedy;
pub use objective::{Objective, ScaledOps, BulkInsertion};
pub use vector::*;
pub use noisy_float::prelude::*;
