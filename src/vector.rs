// use std::collections::BTreeMap;
use std::ops::{Index, Deref, DerefMut, AddAssign, Mul, Add};

/// An empty vector.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct NullVector;
/// The `s`th Unit Vector
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct UnitVector(usize);
/// A scaled unit vector (e.g. `l * s_i`)
///
/// These can be constructed from a `UnitVector` by multiplication, e.g.
///
/// ```rust
/// use lace::prelude::*;
///
/// let si = unit(52);
/// let scale = 3 * si;
/// assert_eq!(scale[52], 3);
/// assert_eq!(scale.cardinality(), 3);
/// ```
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct ScaleVector {
    s: usize,
    scale: usize,
}
// type SparseSet = BTreeMap<usize, usize>;
// /// A sparse vector, represented as a set at the moment.
// #[derive(Clone, Debug)]
// pub struct SparseVector(SparseSet);
/// A dense vector, represented as a full vector.
///
/// Even if the domain is not contiguous, this representation is.
///
/// Other `Vector` types may be added to a `DenseVector`, but not visa-versa. So, the following
/// works:
///
/// ```rust 
/// use lace::prelude::*;
///
/// let mut dense = DenseVector::new(100);
/// dense += 3 * unit(52);
/// assert_eq!(dense[52], 3);
/// ```
///
/// But this does not compile:
///
/// ```rust,ignore
/// use lace::prelude::*;
/// let mut u = 3 * unit(52);
/// u += DenseVector::new(100);
/// ```
#[derive(Clone, Debug, PartialEq, Hash, Eq)]
pub struct DenseVector(Vec<usize>);

/// Common operations on all vectors.
///
/// Note that all `Vector` implementations also implement `Index<usize>`, so bracket-indexing
/// always works, even if you only know you have an implementor and not *which* implementor.
///
/// # Example
///
/// ```rust
/// use lace::prelude::*;
///
/// fn trait_obj<V: Vector>(obj: &V) {
///     assert_eq!(obj[52], 3);
/// }
///
/// let vec = 3 * unit(52);
/// trait_obj(&vec);
///
/// let mut dense = DenseVector::new(100);
/// dense += vec;
///
/// trait_obj(&vec);
/// ```
pub trait Vector: Index<usize, Output=usize> {
    /// Tests whether the vector has element `s_i` > 0.
    fn contains(&self, i: usize) -> bool {
        self.get(i) > 0
    }

    /// Get the count of element `i`.
    fn get(&self, i: usize) -> usize;

    /// The sum of counts of all elements in the vector.
    fn cardinality(&self) -> usize;
}

/// Operations on mutable vectors (`UnitVector`s and `ScaleVector`s are immutable).
pub trait VectorMut {
    /// Update the count of `i` to `val`.
    fn set(&mut self, i: usize, val: usize);
}

//////////////////////
// NullVector Impls //
//////////////////////
impl Vector for NullVector {
    fn contains(&self, _: usize) -> bool { false }
    fn get(&self, _: usize) -> usize { 0 }
    fn cardinality(&self) -> usize { 0 }
}

impl Index<usize> for NullVector {
    type Output = usize;

    fn index(&self, _: usize) -> &usize {
        &0
    }
}

//////////////////////
// UnitVector Impls //
//////////////////////

/// Convenience constructor for the `i`th `UnitVector`.
pub fn unit(i: usize) -> UnitVector {
    UnitVector::new(i)
}

impl UnitVector {
    pub fn new(i: usize) -> Self {
        UnitVector(i)
    }

    pub fn scale(&self, scale: usize) -> ScaleVector {
        ScaleVector { s: self.0, scale }
    }

    pub fn el(&self) -> usize {
        self.0
    }
}

impl Vector for UnitVector {
    fn contains(&self, i: usize) -> bool {
        self.0 == i
    }

    fn get(&self, i: usize) -> usize {
        if self.0 == i { 1 } else { 0 }
    }
    
    fn cardinality(&self) -> usize { 1 }
}

impl Index<usize> for UnitVector {
    type Output = usize;

    fn index(&self, i: usize) -> &usize {
        if self.0 == i { &1 } else { &0 }
    }
}

impl Mul<usize> for UnitVector {
    type Output = ScaleVector;
    fn mul(self, scale: usize) -> ScaleVector {
        self.scale(scale)
    }
}
impl Mul<UnitVector> for usize {
    type Output = ScaleVector;
    fn mul(self, unit: UnitVector) -> ScaleVector {
        unit.scale(self)
    }
}
///////////////////////
// ScaleVector Impls //
///////////////////////
impl ScaleVector {
    pub fn new(s: usize, scale: usize) -> Self {
        ScaleVector { s, scale }
    }

    pub fn scale(&self, scale: usize) -> Self {
        ScaleVector {
            scale: scale * self.scale,
            ..*self
        }
    }
    
    pub fn el(&self) -> usize {
        self.s
    }
}

impl Vector for ScaleVector {
    fn contains(&self, i: usize) -> bool {
        self.s == i
    }

    fn get(&self, i: usize) -> usize {
        if self.s == i { self.scale } else { 0 }
    }

    fn cardinality(&self) -> usize {
        self.scale
    }
}

impl Index<usize> for ScaleVector {
    type Output = usize;

    fn index(&self, i: usize) -> &usize {
        if self.s == i { &self.scale } else { &0 }
    }
}

///////////////////////
// DenseVector Impls //
///////////////////////
impl DenseVector {
    pub fn new(size: usize) -> Self {
        DenseVector(vec![0; size])
    }
}

impl From<Vec<usize>> for DenseVector {
    fn from(vec: Vec<usize>) -> Self {
        DenseVector(vec)
    }
}

impl Vector for DenseVector {
    fn contains(&self, i: usize) -> bool {
        self.0[i] > 0
    }

    fn get(&self, i: usize) -> usize {
        self.0[i]
    }

    fn cardinality(&self) -> usize {
        self.0.iter().sum()
    }
}

impl VectorMut for DenseVector {
    fn set(&mut self, i: usize, val: usize) {
        self.0[i] = val;
    }
}

impl Index<usize> for DenseVector {
    type Output = usize;
    fn index(&self, i: usize) -> &usize {
        self.0.index(i)
    }
}

impl Deref for DenseVector {
    type Target = Vec<usize>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DenseVector {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Add for DenseVector {
    type Output = Self;
    fn add(mut self, rhs: Self) -> Self {
        assert_eq!(
            self.0.len(),
            rhs.0.len(),
            "dense vectors must have equal length to add"
        );
        for (left, right) in self.0.iter_mut().zip(rhs.0) {
            *left += right;
        }
        self
    }
}

impl AddAssign for DenseVector {
    fn add_assign(&mut self, rhs: Self) {
        assert_eq!(
            self.0.len(),
            rhs.0.len(),
            "dense vectors must have equal length to add"
        );
        for (left, &right) in self.0.iter_mut().zip(&rhs.0) {
            *left += right;
        }
    }
}

impl Add<UnitVector> for DenseVector {
    type Output = Self;
    fn add(mut self, UnitVector(i): UnitVector) -> Self {
        self.0[i] += 1;
        self
    }
}

impl AddAssign<UnitVector> for DenseVector {
    fn add_assign(&mut self, UnitVector(i): UnitVector) {
        self.0[i] += 1;
    }
}

impl Add<ScaleVector> for DenseVector {
    type Output = Self;
    fn add(mut self, ScaleVector { s, scale }: ScaleVector) -> Self {
        self.0[s] += scale;
        self
    }
}

impl AddAssign<ScaleVector> for DenseVector {
    fn add_assign(&mut self, ScaleVector { s, scale }: ScaleVector) {
        self.0[s] += scale;
    }
}

impl Add<NullVector> for DenseVector {
    type Output = Self;
    fn add(self, _: NullVector) -> Self {
        self
    }

}
