# Implementation: Fast Maximization of Non-Submodular, Monotonic Functions on the Integer Lattice

    @inproceedings{kuhnle_fast_2018,
      title = {Fast {{Maximization}} of {{Non}}-{{Submodular}}, {{Monotonic Functions}} on the {{Integer Lattice}}},
      booktitle = {Proceedings of the 35th {{International Conference}} on {{Machine Learning}}},
      series = {ICML'18},
      date = {2018},
      author = {Kuhnle, Alan and Smith, J. David and Crawford, Victoria and Thai, My T.}
    }

## Building

This software is implemented in [Rust](https://rust-lang.org). To build it, you
must install the latest stable 1.x version of Rust.

There is an additional dependency on Cap'n Proto. On Debian/Ubuntu, this
dependency can be met by running

    sudo apt-get install libcapnp-dev capnproto

Once the dependencies are installed, the software can be built with

    cargo build --release

And then run with (for example):

    ./target/release/inf ca-GrQc_uniform.bin 100 --det-steps 10 --threads 8 --greedy fast --epsilon 0.05 --delta 0.9 --kappa 0.95

## Usage

Use `./target/release/inf --help` to see the usage information. It is
reproduced below for completeness:

    Run the pseudo-IM simulation. We use the IC model.

    The gamma-brute and gamma-s-brute commands are used to find values of γ_d
    and γ_s, respectively. They are naïve brute-force methods and will not complete
    quickly.

    Usage:
        inf <graph> <k> [options]
        inf gamma-brute <graph> <k> [options]
        inf gamma-s-brute <graph> <k> [options]
        inf (-h | --help)

    Options:
        -h --help               Show this screen.
        --samples <sam>         The number of samples used for Monte Carlo. [default: 10000]
        --threads <t>           The number of threads to use. [default: 1]
        --log <file>            Log output to <file> in JSON lines format.
        --box <constraint>      Bound on the number of times a single element can be chosen. 
                                If not set, an element may be selected an arbitrary number of times.
        --greedy <method>       Greedy method to use. [default: standard]
        --delta <delta>         Parameter for Fast Greedy (description pending).
        --epsilon <eps>         Error margin for the threshold greedy method.
        --det-steps <bmax>      Require <bmax> steps to deterministically seed an element. [default: 10]
        --kappa <kappa>         Step size between thresholds.

## Experiments

The code for the complete experiments found in the paper is contained in
a separate repository: https://gitlab.com/emallson/lace-exp

## License

Copyright (c) 2018, J. David Smith & Alan Kuhnle
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
